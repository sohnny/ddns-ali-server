package club.zykeji.ddns.aliddnsserver;

import club.zykeji.ddns.aliddnsserver.util.DnsUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AliDdnsServerApplicationTests {

    @Autowired
    private DnsUtil dnsUtil;

    @Test
    void contextLoads() {
        System.out.println(dnsUtil.getIpByHost("ddns.zykeji.club"));
    }

}
