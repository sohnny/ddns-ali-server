package club.zykeji.ddns.aliddnsserver.util;

/**
 * Create by 丶TheEnd on 2020/5/5.
 * @author TheEnd
 */
public class StringUtil {

    /**
     * 获取域名的二级域名
     * @param host
     * @return
     */
    public static String getSecondaryDomain(String host) {
        int lastDelimiter = host.lastIndexOf(".", host.lastIndexOf(".") - 1);
        return host.substring(0, lastDelimiter);
    }

}
