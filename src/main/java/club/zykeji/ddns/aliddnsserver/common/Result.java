package club.zykeji.ddns.aliddnsserver.common;

import lombok.Getter;

/**
 * Create by 丶TheEnd on 2020/5/5.
 * @author TheEnd
 */
@Getter
public class Result {

    private Integer code;
    private String msg;

    public static Result error() {
        return error("error");
    }

    public static Result error(String msg) {
        return new Result(-1, msg);
    }

    public static Result success() {
        return success("success");
    }

    public static Result success(String msg) {
        return new Result(200, msg);
    }

    private Result(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
